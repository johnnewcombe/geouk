﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeoUK.OSTN.Tests.Models
{
    public class DataPoint
    {
        public string PointID { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Height { get; set; }
    }
}
